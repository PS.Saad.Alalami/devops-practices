FROM java:8-jdk-alpine
EXPOSE 8090
WORKDIR /app
COPY --chown=nobody:nobody target/*.jar ./app.jar
ENV h2_profile=h2
ENV mysql_profile=mysql
CMD ls && pwd
CMD java -jar -Dserver.port=8090 -Dspring.profiles.active=mysql app.jar