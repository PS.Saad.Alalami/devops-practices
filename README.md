# DevOps Team Assignment

* Fixed Pom file
* packaged my app using maven
* added gitlab-ci file
* added .ignore file
* added pipeline stage
* activated artifact feature for my jar file so it can be downloaded
* added commithash and date for the JAR file naming convension 
* installed gitlab runner on my machine ( SHELL )
* added dockerfile and step in pipeline
* added a bash script for dockerfilee
* added docker-compose file
* installed docker swarm and tested the app using psotman
* added k8s files and deployments.
